const { Schema, model } = require('mongoose');

const schema = new Schema({
  name: {type: String, required: true},
  image: {
    type: Schema.Types.ObjectId,
    ref: 'File',
    required: true,
  },
},
{
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
}
);

const Image = model('Image', schema);
module.exports = Image;

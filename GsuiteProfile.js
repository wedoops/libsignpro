const { Schema, model } = require('mongoose');

const schema = new Schema({
  primaryEmail: {type: String, required: true},
  aliases: [{
    type: Schema.Types.ObjectId,
    ref: 'ProfileCompanyAlias',
    required: true,
  }],
  gsuiteAccount: {
    type: Schema.Types.ObjectId,
    ref: 'GsuiteAccount',
    required: true,
  },
},
{
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
}
);

const GsuiteProfile = model('GsuiteProfile', schema);
GsuiteProfile.findOrCreate = async(data) => await GsuiteProfile.findOne(data) || await GsuiteProfile.create(data);
module.exports = GsuiteProfile;

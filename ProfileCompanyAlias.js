const { Schema, model } = require('mongoose');
const AliasEvent = require('./AliasEvent');

const schema = new Schema({
  profile: {
    type: Schema.Types.ObjectId,
    ref: 'GsuiteProfile',
    required: true,
  },
  company: {
    type: Schema.Types.ObjectId,
    ref: 'Company',
    required: true,
  },
  aliasEmail: {
    type: String,
    unique: true,
    required: true,
  },
  defaultSignatureEnabled: {type: Boolean, default: false},
  events: [{
    type: Schema.Types.ObjectId,
    ref: 'AliasEvent',
    default: [],
  }],
  lastPlannedAt: {
    type: Date,
    default: null,
  },
  currentSignature: {
    type: Schema.Types.ObjectId,
    ref: 'Signature',
    default: null,
  },
},
{
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
}
);

const ProfileCompanyAlias = model('ProfileCompanyAlias', schema);

ProfileCompanyAlias.findOrCreate = async(data) => await ProfileCompanyAlias.findOne(data) || await ProfileCompanyAlias.create(data);
ProfileCompanyAlias.prototype.createEvent = async function(action) {
  console.log('ADD EVENT', action, this.id, this.aliasEmail);
  return await AliasEvent.create({action, profile: this, signature: this.currentSignature});
};

module.exports = ProfileCompanyAlias;

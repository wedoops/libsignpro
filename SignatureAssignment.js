const { Schema, model } = require('mongoose');

const schema = new Schema({
  signature: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Signature',
  },
  alias: {
    type: Schema.Types.ObjectId,
    ref: 'ProfileCompanyAlias',
    required: true,
  },
  status: {
    type: String,
    enum: ['pending','scheduled','active','expired'],
    required: true,
  },
},
{
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
}
);

const SignatureAssignment = model('SignatureAssignment', schema);
module.exports = SignatureAssignment;

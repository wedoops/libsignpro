const { Schema, model } = require('mongoose');

const schema = new Schema({
  gsuiteAccount: {
    type: Schema.Types.ObjectId,
    ref: 'GsuiteAccount',
    required: true,
  },
  primaryDomain: {type: String, required: true},
  companyDomain: {type: String, required: true},
  companyPath: {type: String, unique: true},
  creator: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  defaultSignature: {
    type: Schema.Types.ObjectId,
    ref: 'Signature',
  },
  disclaimers: [{
    type: Schema.Types.ObjectId,
    ref: 'Disclaimer',
    default: [],
  }],
  signatures: [{
    type: Schema.Types.ObjectId,
    ref: 'Signature',
    default: [],
  }],
  banners: [{
    type: Schema.Types.ObjectId,
    ref: 'Banner',
    default: [],
  }],
  metadata: {
    colors: [String],
    companyName: String,
    webpage: String,
    imageUrl: String,
    linkedin: String,
    instagram: String,
    twitter: String,
    facebook: String,
    youtube: String,
  },
},
{
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
}
);

const Company = model('Company', schema);

module.exports = Company;

const { Schema, model } = require('mongoose');

const schema = new Schema({
  name: {type: String, required: true},
  html: {type: String, default: ''},
  schema: {type: String, default: ''},
  from: Number,
  to: Number,
  company: { type: Schema.Types.ObjectId, ref: 'Company', required: true},
  /*
  TODO: items brokes the app with the following error: Cannot read property 'indexedPaths' of undefined
  items: [{
    type: Schema.Types.ObjectId,
    ref: 'SignatureItem',
    default: [],
  }],
  */
},
{
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
}
);

const Signature = model('Signature', schema);
module.exports = Signature;

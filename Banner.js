const { Schema, model } = require('mongoose');

const schema = new Schema({
  track: {type: Boolean, default: false},
  link: {type: String, required: false},
  title: {type: String, required: true},
  company: { type: Schema.Types.ObjectId, ref: 'Company', required: true},
  image: {
    type: Schema.Types.ObjectId,
    ref: 'File',
    required: true,
  },
  signatures: [
    { type: Schema.Types.ObjectId,
      ref: 'SignatureItem',
      default: [],
    },
  ],
},
{
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
}
);

const Banner = model('Banner', schema);
module.exports = Banner;

const { Schema, model } = require('mongoose');

const schema = new Schema({
  text: {type: String, required: true},
  title: {type: String, required: true},
  company: { type: Schema.Types.ObjectId, ref: 'Company', required: true},
  signatures: [
    { type: Schema.Types.ObjectId,
      ref: 'SignatureItem',
      default: [],
    },
  ],
},
{
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
}
);

const Disclaimer = model('Disclaimer', schema);
module.exports = Disclaimer;

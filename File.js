const { Schema, model } = require('mongoose');

const schema = new Schema({
  storageProvider: {type: String, required: true},
  storageReference: {type: String, required: true},
  url: {type: String, required: true},
  hash: {type: String, required: true},
  mimeType: {type: String, required: true},
  size: {type: Number, required: true},
},
{
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
}
);

const File = model('File', schema);
module.exports = File;

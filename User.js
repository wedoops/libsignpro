const { Schema, model } = require('mongoose');

const schema = new Schema({
  userData: Object,
  gsuiteAccount: {
    type: Schema.Types.ObjectId,
    ref: 'GsuiteAccount',
  },
  lastLogin: Date,
},
{
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
}
);

const User = model('User', schema);

User.findOrCreate = async(data) => await User.findOne(data) || await User.create(data);

module.exports = User;

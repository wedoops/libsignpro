const { Schema, model } = require('mongoose');

const schema = new Schema({
  signature: {
    type: Schema.Types.ObjectId,
    ref: 'Signature',
    required: false,
  },
  profile: {
    type: Schema.Types.ObjectId,
    ref: 'ProfileCompanyAlias',
    required: true,
  },
  action: {
    type: String,
    enum: ['assignSignature', 'unassignSignature', 'removeGmailSignature', 'applyGmailSignature', 'disableDefaultSignature', 'enableDefaultSignature'],
    required: true,
  },
},
{
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
}
);

const AliasEvent = model('AliasEvent', schema);
module.exports = AliasEvent;

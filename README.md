# SETUP
https://docs.gitlab.com/ee/user/packages/npm_registry/

To publish a new package upgrade the version in package.json, set ENVS `PROJECT_ID`, `CI_JOB_TOKEN` and run `$ npm publish`

## ENVS
keybase://team/wedoops_sre/signpro/libsignpro-env.txt

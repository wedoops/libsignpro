const { Schema, model } = require('mongoose');

const schema = new Schema({
  account: {type: String, required: true},
  key: {type: String, required: true},
  base64EncodedLocation: {type: String, required: true},
  total: {type: Number, default: 0},
},
{
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
}
);

const TrackCounter = model('TrackCounter', schema);
TrackCounter.findOrCreate = async(data) => await TrackCounter.findOne(data) || await TrackCounter.create(data);

module.exports = TrackCounter;

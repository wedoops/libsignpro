const { Schema, model } = require('mongoose');

const schema = new Schema({
  signature: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Signature',
  },
  modelId: {
    type: Schema.Types.ObjectId,
    required: true,
    refPath: 'onModel',
  },
  onModel: {
    type: String,
    required: true,
    enum: ['Banner', 'Disclaimer'],
  },
},
{
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
}
);

const SignatureItem = model('SignatureItem', schema);
module.exports = SignatureItem;

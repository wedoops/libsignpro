const { Schema, model } = require('mongoose');

const schema = new Schema({
  customerId: {type: String, required: true},
  companies: [{
    type: Schema.Types.ObjectId,
    ref: 'Company',
    default: [],
  }],
  profiles: [{
    type: Schema.Types.ObjectId,
    ref: 'GsuiteProfile',
    default: [],
  }],
},
{
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
}
);

const GsuiteAccount = model('GsuiteAccount', schema);

GsuiteAccount.findOrCreate = async(data) => await GsuiteAccount.findOne(data) || await GsuiteAccount.create(data);

module.exports = GsuiteAccount;
